package com.healthfortis.sample.celltableleak.client;

import java.util.ArrayList;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT;
import com.google.gwt.user.cellview.client.CellTable;
import com.google.gwt.user.cellview.client.TextColumn;
import com.google.gwt.user.client.Timer;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class CellTableLeak implements EntryPoint {

	private VerticalPanel vPanel = new VerticalPanel();
	private ArrayList<SampleRecord> recordList = new ArrayList<SampleRecord>();
	private CellTable<SampleRecord> sampleTable = new CellTable<SampleRecord>();
	private static int round = 0;
	
	/**
	 * Create a remote service proxy to talk to the server-side Greeting service.
	 */
	@SuppressWarnings("unused")
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		RootPanel.get("sampleTable").add(vPanel);
		Timer refreshTimer = new Timer() {
			@Override
			public void run() {
				refreshTable();
				schedule(10); // repeat every 10 ms to show rapid memory usage
			}
		};
		refreshTimer.run();
	}
	
	private void refreshTable() {
		++round;
		if (vPanel.getWidgetCount() == 0) {
			// First time refreshing.
			initializeSampleTable();
			vPanel.add(sampleTable);
		}
		setData();
	}
	
	private void initializeSampleTable() {
		TextColumn<SampleRecord> stringColumn = new TextColumn<SampleRecord>() {
			@Override
			public String getValue(SampleRecord object) {
				if (object == null) {
					return "";
				}
				return object.getStringField();
			}
		};
		TextColumn<SampleRecord> intColumn = new TextColumn<SampleRecord>() {
			@Override
			public String getValue(SampleRecord object) {
				if (object == null) {
					return "";
				}
				return Integer.toString(object.getIntField());
			}
		};
		sampleTable.addColumn(stringColumn, "String Column");
		sampleTable.addColumn(intColumn, "Integer Column");
	}
	
	private void setData() {
		recordList.clear();
		for (int index = 0; index < 20; ++index) {
			SampleRecord record = new SampleRecord("Record " + Integer.toString(round) + Integer.toString(index), index);
			recordList.add(record);
		}
		
        sampleTable.setRowCount(recordList.size(), true);

		// Push the data into the widget. This seems to be causing the memory leak.
        sampleTable.setRowData(0, recordList);
	}
}
