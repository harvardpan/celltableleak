package com.healthfortis.sample.celltableleak.client;

import java.io.Serializable;

public class SampleRecord implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String stringField;
	private Integer intField;
	
	public SampleRecord() {
	}

	public SampleRecord(String stringField, Integer intField) {
		super();
		this.stringField = stringField;
		this.intField = intField;
	}

	public String getStringField() {
		return stringField;
	}

	public void setStringField(String stringField) {
		this.stringField = stringField;
	}

	public Integer getIntField() {
		return intField;
	}

	public void setIntField(Integer intField) {
		this.intField = intField;
	}
}
